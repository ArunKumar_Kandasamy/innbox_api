class ChangeNameFromUsers < ActiveRecord::Migration
  def change
       remove_column :users, :name
       add_column :users, :domain_name, :string
  end
end

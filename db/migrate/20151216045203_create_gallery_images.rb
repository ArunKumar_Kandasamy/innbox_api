class CreateGalleryImages < ActiveRecord::Migration
  def change
    create_table :gallery_images do |t|
      t.attachment :avatar
      t.integer :gallery_type_id
      t.string :alt_text
      t.integer :sort_order
      t.string :link
      t.text :description
      t.string :title

      t.timestamps null: false
    end
  end
end

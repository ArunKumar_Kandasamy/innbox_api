class CreateCmsPages < ActiveRecord::Migration
  def change
    create_table :cms_pages do |t|
      t.string :page_name
      t.string :title
      t.text :description

      t.timestamps null: false
    end
  end
end

class CreateGalleryTypes < ActiveRecord::Migration
  def change
    create_table :gallery_types do |t|
      t.string :name
      t.integer :height
      t.integer :width
      t.boolean :is_video, :default =>  false
      t.string :image_type

      t.timestamps null: false
    end
  end
end

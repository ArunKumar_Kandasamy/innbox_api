class RemoveColumnVisitsFromUsers < ActiveRecord::Migration
      def change
            remove_column :users, :visits
            add_column :users, :visits, :integer, default: 0
      end
end

json.array!(@settings) do |setting|
  json.extract! setting, :id, :verify_user
  json.url setting_url(setting, format: :json)
end

class Api::V1::UserSerializer < Api::V1::BaseSerializer

    attributes :id, :email, :domain_name,  :activated, :admin, :created_at, :updated_at,:activated_at,:access_token,:activation_token,:verified, :visits

end

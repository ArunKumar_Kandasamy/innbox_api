class Api::V1::Consumer::GalleryImageSerializer < Api::V1::Consumer::ConsumerSerializer
     attributes :id, :avatar, :gallery_type_id, :alt_text, :sort_order, :link, :description, :title, :tag_list
end
 
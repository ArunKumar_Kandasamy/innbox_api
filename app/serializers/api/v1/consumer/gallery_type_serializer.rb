class Api::V1::Consumer::GalleryTypeSerializer < Api::V1::Consumer::ConsumerSerializer
    attributes :id, :name, :height, :width, :is_video, :image_type
    has_many :gallery_images, serializer: Api::V1::Consumer::GalleryImageSerializer
end

class Api::V1::Consumer::CmsPageSerializer < Api::V1::Consumer::ConsumerSerializer
  attributes :id, :page_name, :title, :description
end

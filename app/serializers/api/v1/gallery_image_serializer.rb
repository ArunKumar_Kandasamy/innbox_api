class Api::V1::GalleryImageSerializer < Api::V1::BaseSerializer
     attributes :id, :avatar, :gallery_type_id, :alt_text, :sort_order, :link, :description, :title, :crop_x, :crop_y, :crop_w, :crop_h, :tag_list
end
 
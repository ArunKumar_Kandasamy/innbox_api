class Api::V1::GalleryTypeSerializer < Api::V1::BaseSerializer
      attributes :id, :name, :height, :width, :is_video, :image_type
      has_many :gallery_images
end

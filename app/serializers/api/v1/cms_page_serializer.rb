class Api::V1::CmsPageSerializer < Api::V1::BaseSerializer
  attributes :id, :page_name, :title, :description
end

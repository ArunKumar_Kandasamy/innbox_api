class Api::V1::UsersController < Api::V1::BaseController
      wrap_parameters :user, include: [:domain_name, :email, :password, :password_confirmation, :verified]
      before_filter :authenticate_user!, only: [:show, :update, :destroy]
      before_filter :check_tenant!, only: [:show, :update, :destroy]

      def index
            if params[:follower_id]
              users = User.find(params[:follower_id]).followers
            elsif params[:following_id]
              users = User.find(params[:following_id]).following
            else
              users = User.all.order(created_at: :asc)
            end
            users = apply_filters(users, params)
            
            users = paginate(users)
            
            users = policy_scope(users)
            
            render(
              json: ActiveModel::ArraySerializer.new(
                users,
                each_serializer: Api::V1::UserSerializer,
                root: 'users',
                meta: meta_attributes(users)
              )
            )
      end

      def show
            user = User.find(params[:id])
            authorize user
            unless @current_user.admin?
                  user = @current_user
            end
            render(json: Api::V1::UserSerializer.new(user).to_json)
      end

      def create
            user = User.new(create_params)
            return api_error(status: 422, errors: user.errors) unless user.valid?
            user.save!
            unless verify_user_setting
                 user.send_activation_email
                 Apartment::Tenant.create(user.tenant_name)
            end
            render(
              json: Api::V1::UserSerializer.new(user).to_json,
              status: 201,
              location: api_v1_user_path(user.id)
            )
      end

      def update
            user = User.find(params[:id])
            authorize user
            if verify_user_setting && user.activated != true
                  unless user.verified == true && params[:user][:verified] == true 
                      activation_token  = User.new_token
                      user.activation_token  = activation_token
                      user.activation_digest = User.digest(activation_token)
                      user.save
                      user.send_activation_email
                      Apartment::Tenant.create(user.tenant_name)
                  end
            end
            @tenant_name = user.tenant_name
            if !user.update_attributes(update_params)
              return api_error(status: 422, errors: user.errors)
            end
            if user.tenant_name != @tenant_name
                sql = "ALTER SCHEMA #{@tenant_name} RENAME TO #{user.tenant_name};"
                records_array = ActiveRecord::Base.connection.execute(sql)
                Apartment::Tenant.switch!(user.tenant_name)
            end
            render(
              json: Api::V1::UserSerializer.new(user).to_json,
              status: 200,
              location: api_v1_user_path(user.id),
              serializer: Api::V1::UserSerializer
            )
      end

      def destroy
            user = User.find(params[:id])
            @tenant_name = user.tenant_name
            authorize user
            if user.verified == true
              Apartment::Tenant.drop(@tenant_name)
            end
            if !user.destroy
              return api_error(status: 500)
            end
            head status: 204
      end

      private

            def create_params
                  params.require(:user).permit(
                    :email, :password, :password_confirmation, :domain_name, :tenant_name, :verified, :visits
                  )
            end

            def update_params
                 create_params
            end
end

class Api::V1::CmsPagesController < Api::V1::BaseController
      before_filter :authenticate_user!
      before_action :set_cms_page, only: [:show, :edit, :update, :destroy]
      # GET /cms_pages
      # GET /cms_pages.json
      def index
        @cms_pages = CmsPage.all
        render(
          json: ActiveModel::ArraySerializer.new(
            @cms_pages,
            each_serializer: Api::V1::CmsPageSerializer,
            root: 'cms_pages'
          )
        )
      end

      # GET /cms_pages/1
      # GET /cms_pages/1.json
      def show
        render(json: Api::V1::CmsPageSerializer.new(@cms_page).to_json)
      end

      # GET /cms_pages/new
      def new
        @cms_page = CmsPage.new
      end

      # GET /cms_pages/1/edit
      def edit
      end

      # POST /cms_pages
      # POST /cms_pages.json
      def create
        @cms_page = CmsPage.new(cms_page_params)
        return api_error(status: 422, errors: @cms_page.errors) unless @cms_page.valid?
        @cms_page.save!
        render(
          json: Api::V1::CmsPageSerializer.new(@cms_page).to_json,
          status: 201,
          location: api_v1_cms_page_path(@cms_page)
        )
      end

      # PATCH/PUT /cms_pages/1
      # PATCH/PUT /cms_pages/1.json
      def update
        respond_to do |format|
          if @cms_page.update(cms_page_params)
            format.html { redirect_to @cms_page, notice: 'Cms page was successfully updated.' }
            format.json { head :no_content }
          else
            format.html { render action: 'edit' }
            format.json { render json: @cms_page.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /cms_pages/1
      # DELETE /cms_pages/1.json
      def destroy
        @cms_page.destroy
        respond_to do |format|
          format.html { redirect_to cms_pages_url }
          format.json { head :no_content }
        end
      end

      private

        def switch_tenant
            if @current_user
                if Apartment::Tenant.current != @current_user.tenant_name
                  Apartment::Tenant.switch!(@current_user.tenant_name)
                elsif @current_user
                  Apartment::Tenant.switch! # Revert to the primary tenant
                end
            end
        end

        # Use callbacks to share common setup or constraints between actions.
        def set_cms_page
          @cms_page = CmsPage.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def cms_page_params
          params.require(:cms_page).permit(:page_name, :title, :description)
        end
end

class Api::V1::GalleryTypesController < Api::V1::BaseController
      before_filter :authenticate_user!
      before_action :set_gallery_type, only: [:show, :edit, :update, :destroy]

      # GET /gallery_types
      # GET /gallery_types.json
      def index
           @gallery_types = GalleryType.all
            render(
                      json: ActiveModel::ArraySerializer.new(
                        @gallery_types,
                        each_serializer: Api::V1::GalleryTypeSerializer,
                        root: 'gallery_types'
                  )
            )
      end

      # GET /gallery_types/1
      # GET /gallery_types/1.json
      def show
           render(json: Api::V1::GalleryTypeSerializer.new(@gallery_type).to_json)
      end

      # GET /gallery_types/new
      def new
            @gallery_type = GalleryType.new
      end

      # GET /gallery_types/1/edit
      def edit
      end

      # POST /gallery_types
      # POST /gallery_types.json
      def create
            @gallery_type = GalleryType.new(gallery_type_params)
            return api_error(status: 422, errors: @gallery_type.errors) unless @gallery_type.valid?
            @gallery_type.save!
            render(
                json: Api::V1::GalleryTypeSerializer.new(@gallery_type).to_json,
                status: 201,
                location: api_v1_gallery_type_path(@gallery_type)
            )
      end

      # PATCH/PUT /gallery_types/1
      # PATCH/PUT /gallery_types/1.json
      def update
            respond_to do |format|
                   if @gallery_type.update(gallery_type_params)
                        format.html { redirect_to @gallery_type, notice: 'Gallery type was successfully updated.' }
                        format.json { head :no_content }
                   else
                        format.html { render action: 'edit' }
                        format.json { render json: @gallery_type.errors, status: :unprocessable_entity }
                   end
            end
      end

      # DELETE /gallery_types/1
      # DELETE /gallery_types/1.json
      def destroy
            @gallery_type.destroy
            respond_to do |format|
                  format.html { redirect_to gallery_types_url }
                  format.json { head :no_content }
            end
      end

      private
        # Use callbacks to share common setup or constraints between actions.
       def set_gallery_type
            @gallery_type = GalleryType.find(params[:id])
      end

        # Never trust parameters from the scary internet, only allow the white list through.
      def gallery_type_params
            params.require(:gallery_type).permit(:name, :height, :width, :is_video, :image_type)
      end
end

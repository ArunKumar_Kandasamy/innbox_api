class Api::V1::SettingsController < Api::V1::BaseController
      before_action :set_setting, only: [:show, :edit, :update, :destroy]

      # GET /settings
      # GET /settings.json
      def index
             @settings = Setting.all
             render(
              json: ActiveModel::ArraySerializer.new(
                @settings,
                each_serializer: Api::V1::SettingSerializer,
                root: 'settings'
              )
            )
      end

      # GET /settings/1
      # GET /settings/1.json
      def show
           render(json: Api::V1::SettingSerializer.new(@setting).to_json)
      end

      # GET /settings/new
      def new
        @setting = Setting.new
      end

      # GET /settings/1/edit
      def edit
      end

      # POST /settings
      # POST /settings.json
      def create
            @setting = Setting.new(setting_params)
            return api_error(status: 422, errors: @setting.errors) unless @setting.valid?
            @setting.save!
            render(
                  json: Api::V1::SettingSerializer.new(@setting).to_json,
                  status: 201,
                  location: api_v1_setting_path(@setting.id)
            )
      end

      # PATCH/PUT /settings/1
      # PATCH/PUT /settings/1.json
      def update
        respond_to do |format|
          if @setting.update(setting_params)
            format.html { redirect_to @setting, notice: 'Setting was successfully updated.' }
            format.json { head :no_content }
          else
            format.html { render action: 'edit' }
            format.json { render json: @setting.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /settings/1
      # DELETE /settings/1.json
      def destroy
            @setting.destroy
            respond_to do |format|
                format.html { redirect_to settings_url }
                format.json { head :no_content }
            end
      end

      private
             # Use callbacks to share common setup or constraints between actions.
            def set_setting
              @setting = Setting.find(params[:id])
            end

            # Never trust parameters from the scary internet, only allow the white list through.
            def setting_params
              params.require(:setting).permit(:verify_user)
            end
end

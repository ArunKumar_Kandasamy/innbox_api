class Api::V1::SessionsController < Api::V1::BaseController

  def create
      user = User.find_by(email: create_params[:email])
      if user && user.verified? && user.activated? && user.authenticate(create_params[:password])
        self.current_user = user
        user.visits = user.visits + 1
        user.save
        render(
          json: Api::V1::SessionSerializer.new(user, root: false).to_json,
          status: 201
        )
      else
        if !user
          render json: { error: { messages: "Invalid email"} }, status: 422
        elsif user.authenticate(create_params[:password])
          render json: { error: { messages: "Invalid password"} }, status: 422
        elsif user.verified?
          render json: { error: { messages: "User not yet verified"} }, status: 422
        elsif user.activated?
          render json: { error: { messages: "User not yet activated"} }, status: 422
        end
      end
  end

  private
  
  def create_params
    params.require(:session).permit(:email, :password)
  end
end

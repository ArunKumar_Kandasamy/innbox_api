class Api::V1::AccountActivationsController < Api::V1::BaseController
  def edit
    user = User.find_by(email: params[:email])
    if user && !user.activated? && user.authenticated?(:activation, params[:id])
      user.activate
      log_in user
      flash[:success] = "Account activated!"
      redirect_to user
    else
      flash[:danger] = "Invalid activation link"
      redirect_to root_url
    end
  end

  def create
      user = User.find_by(email: params[:email])
      if user && !user.activated? && user.authenticated?(:activation, params[:id])
            user.activate
            render(
                json: Api::V1::UserSerializer.new(user).to_json,
                status: 201,
                location: api_v1_user_path(user.id)
            )
      else
            return api_error(status: 401)
      end
  end
end

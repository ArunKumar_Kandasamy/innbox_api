class Api::V1::GalleryImagesController < Api::V1::BaseController
      wrap_parameters :gallery_image, include: [:gallery_type_id, :alt_text, :sort_order, :link, :description, :title, :crop_x, :crop_y, :crop_w, :crop_h, :tag_list]
      before_filter :authenticate_user!
      before_action :set_gallery_image, only: [:show, :edit, :update, :destroy]
      before_action :set_gallery_type,only: [:show, :edit, :update, :index, :create]

      # GET /gallery_images
      # GET /gallery_images.json
      def index
            @gallery_images = @gallery_type.gallery_images
            render(
                  json: ActiveModel::ArraySerializer.new(
                    @gallery_images,
                    each_serializer: Api::V1::GalleryImageSerializer,
                    root: 'gallery_images'
                  )
            )
      end

      # GET /gallery_images/1
      # GET /gallery_images/1.json
      def show
           render(json: Api::V1::GalleryImageSerializer.new(@gallery_image).to_json)
      end

      # GET /gallery_images/new
      def new
        @gallery_image = GalleryImage.new
      end

      # GET /gallery_images/1/edit
      def edit
      end

      # POST /gallery_images
      # POST /gallery_images.json
      def create
            uploaded_io = params[:avatar][:original_filename]
            f = File.open(Rails.root.join('public', 'uploads', uploaded_io.gsub(" ","_")))  
            @gallery_image = GalleryImage.new(gallery_image_params)
            return api_error(status: 422, errors: @gallery_image.errors) unless @gallery_image.valid?
            @gallery_image.avatar = f
            File.delete(Rails.root.join('public', 'uploads', uploaded_io.gsub(" ","_")))   
            @gallery_image.save!
            render(
                  json: Api::V1::GalleryImageSerializer.new(@gallery_image).to_json,
                  status: 201,
                  location: api_v1_gallery_image_path(@gallery_image)
            )
      end

      # PATCH/PUT /gallery_images/1
      # PATCH/PUT /gallery_images/1.json
      def update
        respond_to do |format|
          if @gallery_image.update(gallery_image_params)
             if  params[:avatar].class != String

                  uploaded_io = params[:avatar][:original_filename]
                  f = File.open(Rails.root.join('public', 'uploads', uploaded_io.gsub(" ","_"))) 
                  @gallery_image.avatar = f
                 @gallery_image.save 
              end
             
            format.html { redirect_to @gallery_image, notice: 'Gallery image was successfully updated.' }
            format.json { head :no_content }
          else
            format.html { render action: 'edit' }
            format.json { render json: @gallery_image.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /gallery_images/1
      # DELETE /gallery_images/1.json
      def destroy
        @gallery_image.destroy
        respond_to do |format|
          format.html { redirect_to gallery_images_url }
          format.json { head :no_content }
        end
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_gallery_type
          @gallery_type = GalleryType.find(params[:gallery_type_id])
        end

        # Use callbacks to share common setup or constraints between actions.
        def set_gallery_image
          @gallery_image = GalleryImage.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def gallery_image_params
          params.require(:gallery_image).permit(:avatar, :gallery_type_id, :alt_text, :sort_order, :link, :description, :title, :crop_x, :crop_y, :crop_w, :crop_h, :tag_list)
        end
end

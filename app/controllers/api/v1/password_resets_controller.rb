class Api::V1::PasswordResetsController < Api::V1::BaseController
    before_action :get_user,         only: [:edit, :update]
    before_action :valid_user,       only: [:edit, :update]
    before_action :check_expiration, only: [:edit, :update]

      def new
      end

      def create
            if params[:uid].present?
                  @user = User.find(params[:uid])
                  if  @user.update_attribute(:password,params[:password])
                            render(json: Api::V1::UserSerializer.new(@user).to_json)
                    else
                          return api_error(status: 402)
                   end   
            elsif  params[:token].present?
                  user = User.find_by(email: params[:password_reset][:email].downcase)
                   if user
                         render(
                                json: Api::V1::UserSerializer.new(user).to_json,
                                status: 201,
                                location: api_v1_user_path(user.id)
                              )
                  else
                          return api_error(status: 401)
                   end 
            elsif params[:password_reset].present?
                  @user = User.find_by(email: params[:password_reset][:email].downcase)
                  if @user
                        @user.create_reset_digest
                        @user.send_password_reset_email
                        render(
                                json: Api::V1::UserSerializer.new(@user).to_json,
                                status: 201,
                                location: api_v1_user_path(@user.id)
                              )
                  else
                        return email_not_found
                  end
            else
                   user = User.find_by(email: params[:email])
                   if (user && user.authenticated?(:reset, params[:id]))
                         render(json: Api::V1::UserSerializer.new(user).to_json)
                  else
                          return api_error(status: 401)
                   end   
                   
            end
      end
  
      def edit
          
      end

      def show        
             user = User.find(params[:id]) 
             render(json: Api::V1::UserSerializer.new(user).to_json)
      end
      
      def update
            if both_passwords_blank?
                  flash.now[:danger] = "Password/confirmation can't be blank"
                  render 'edit'
            elsif @user.update_attributes(user_params)
                  log_in @user
                  flash[:success] = "Password has been reset."
                  redirect_to @user
            else
                  render 'edit'
          end
      end
  
  private
    
      def user_params
        params.require(:user).permit(:password, :password_confirmation,:id)
      end
      
      # Returns true if password & confirmation are blank.
      def both_passwords_blank?
        params[:user][:password].blank? && 
        params[:user][:password_confirmation].blank?
      end
    
      # Before filters
    
      def get_user
        @user = User.find_by(email: params[:email])
      end
      
      # Confirms a valid user.
      def valid_user
            unless (@user && @user.activated? && 
                  @user.authenticated?(:reset, params[:id]))
                  redirect_to root_url
            end      
      end
      
      # Checks expiration of reset token.
      def check_expiration
            if @user.password_reset_expired?
                flash[:danger] = "Password reset has expired."
                redirect_to new_password_reset_url
            end
      end
end

class Api::V1::Consumer::GalleryImagesController < Api::V1::Consumer::FoundationController
      before_filter :authenticate_user!
      before_action :set_gallery_image, only: [:show]
      before_action :set_gallery_type,only: [:show,:index]

      def index
           if params[:tag_name].present?
               @gallery_images = @tag.gallery_images
           else
                @gallery_images = @gallery_type.gallery_images
           end
           render(
                      json: ActiveModel::ArraySerializer.new(
                        @gallery_images,
                        each_serializer: Api::V1::Consumer::GalleryImageSerializer,
                        root: 'gallery_images'
                      )
                )
      end

      def show
           render(json: Api::V1::Consumer::GalleryImageSerializer.new(@gallery_image).to_json)
      end


      private
          # Use callbacks to share common setup or constraints between actions.
            def set_gallery_type
                 if params[:tag_name].present?
                     @tag = Tag.find_by_name(params[:tag_name])
                 else
                     @gallery_type = GalleryType.find(params[:gallery_type_id])
                 end
            end

          # Use callbacks to share common setup or constraints between actions.
          def set_gallery_image
            @gallery_image = GalleryImage.find(params[:id])
          end

          # Never trust parameters from the scary internet, only allow the white list through.
          def gallery_image_params
            params.require(:gallery_image).permit(:avatar, :gallery_type_id, :alt_text, :sort_order, :link, :description, :title, :crop_x, :crop_y, :crop_w, :crop_h, :tag_list)
          end
end

class Api::V1::Consumer::FoundationController < ApplicationController
      include Pundit
      include ActiveHashRelation

      # Prevent CSRF attacks by raising an exception.
      # For APIs, you may want to use :null_session instead.
      protect_from_forgery with: :null_session
      before_action :destroy_session
      before_filter :for_access
      rescue_from ActiveRecord::RecordNotFound, with: :not_found!
      rescue_from Pundit::NotAuthorizedError, with: :unauthorized!

      attr_accessor :current_user
      def for_access
        headers['Access-Control-Allow-Origin'] = '*'
        headers['Access-Control-Allow-Methods'] = 'POST, PUT, DELETE, GET, OPTIONS'
        headers['Access-Control-Request-Method'] = '*'
        headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
      end

      protected

      def destroy_session
        request.session_options[:skip] = true
      end

      def unauthenticated!
        response.headers['WWW-Authenticate'] = "Token realm=Application"
        render json: { error: 'Bad credentials' }, status: 401
      end

      def error_found
        render json: { error: { message: "Invalid email or password"} }, status: 422
      end

      def email_not_found
        render json: { error: { message: "Invalid email"} }, status: 422
      end

      def unauthorized!
        render json: { error: 'not authorized' }, status: 403
      end

      def invalid_resource!(errors = [])
        api_error(status: 422, errors: errors)
      end

      def not_found!
        return api_error(status: 404, errors: 'Not found')
      end

      def api_error(status: 500, errors: [])
        unless Rails.env.production?
          puts errors.full_messages if errors.respond_to? :full_messages
        end
        head status: status and return if errors.empty?

        render json: jsonapi_format(errors).to_json, status: status
      end

      #expects pagination!
      def meta_attributes(object)
        {
          current_page: object.current_page,
          next_page: object.next_page,
          prev_page: object.previous_page,
          total_pages: object.total_pages,
          total_count: object.total_entries
        }
      end

      def authenticate_user!
          authenticate_with_http_token do |token, options|
              @user = User.find_by(access_token: token)
          end
          if @user
            @current_user = @user
             if Apartment::Tenant.current != @current_user.tenant_name
                Apartment::Tenant.switch!(@current_user.tenant_name)
              elsif @current_user
                Apartment::Tenant.switch!# Revert to the primary tenant
              end
          else
            return unauthenticated!
          end
      end

      def check_tenant!
          if @current_user
                if Apartment::Tenant.current != @current_user.tenant_name
                  Apartment::Tenant.switch!(@current_user.tenant_name)
                elsif @current_user
                  Apartment::Tenant.switch! # Revert to the primary tenant
                end
          else
            error
          end
      end

      private

      #ember specific :/
      def jsonapi_format(errors)
          return errors if errors.is_a? String
          errors_hash = {}
          errors.messages.each do |attribute, error|
            array_hash = []
            error.each do |e|
              array_hash << {attribute: attribute, message: e}
            end
            errors_hash.merge!({ attribute => array_hash })
          end

          return errors_hash
      end
end

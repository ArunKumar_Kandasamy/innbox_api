class Api::V1::Consumer::GalleryTypesController < Api::V1::Consumer::FoundationController
      before_filter :authenticate_user!
      before_action :set_gallery_type, only: [:show]

      def index
           if params[:tag_id]
                @tags = Tag.all
           else
                @gallery_types = GalleryType.all
                 render(
                            json: ActiveModel::ArraySerializer.new(
                              @gallery_types,
                              each_serializer: Api::V1::Consumer::GalleryTypeSerializer,
                              root: 'gallery_types'
                      )
                 )
           end
      end

      def show
           render(json: Api::V1::Consumer::GalleryTypeSerializer.new(@gallery_type).to_json)
      end

      private
        # Use callbacks to share common setup or constraints between actions.
       def set_gallery_type
            @gallery_type = GalleryType.includes(:gallery_images).find(params[:id])

      end

        # Never trust parameters from the scary internet, only allow the white list through.
      def gallery_type_params
            params.require(:gallery_type).permit(:name, :height, :width, :is_video, :image_type)
      end
end

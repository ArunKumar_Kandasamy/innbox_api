class Api::V1::Consumer::CmsPagesController < Api::V1::Consumer::FoundationController
      before_filter :authenticate_user!
      before_action :set_cms_page, only: [:show]


      def index
        @cms_pages = CmsPage.all
        render(
          json: ActiveModel::ArraySerializer.new(
            @cms_pages,
            each_serializer: Api::V1::Consumer::CmsPageSerializer,
            root: 'cms_pages'
          )
        )
      end

      def show
        render(json: Api::V1::Consumer::CmsPageSerializer.new(@cms_page).to_json)
      end


      private

            def switch_tenant
                if @current_user
                    if Apartment::Tenant.current != @current_user.tenant_name
                      Apartment::Tenant.switch!(@current_user.tenant_name)
                    elsif @current_user
                      Apartment::Tenant.switch! # Revert to the primary tenant
                    end
                end
            end

            # Use callbacks to share common setup or constraints between actions.
            def set_cms_page
              @cms_page = CmsPage.find(params[:id])
            end
end

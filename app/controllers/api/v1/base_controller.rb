class Api::V1::BaseController < ApplicationController
      include Pundit
      include ActiveHashRelation

      # Prevent CSRF attacks by raising an exception.
      # For APIs, you may want to use :null_session instead.
      skip_before_filter :verify_authenticity_token, :if => Proc.new { |c| c.request.format == 'application/json' }
      before_action :destroy_session
      rescue_from ActiveRecord::RecordNotFound, with: :not_found!
      rescue_from Pundit::NotAuthorizedError, with: :unauthorized!

      attr_accessor :current_user
      protected

      def verify_user_setting
          if Setting.first.present?
              return Setting.first.verify_user ? true : false
          else
              return false
          end
      end

      def destroy_session
        request.session_options[:skip] = true
      end

      def unauthenticated!
        response.headers['WWW-Authenticate'] = "Token realm=Application"
        render json: { error: 'Bad credentials' }, status: 401
      end

      def error_found
        render json: { error: { message: "Invalid email or password"} }, status: 422
      end

      def email_not_found
        render json: { error: { message: "Invalid email"} }, status: 422
      end

      def unauthorized!
        render json: { error: 'not authorized' }, status: 403
      end

      def invalid_resource!(errors = [])
        api_error(status: 422, errors: errors)
      end

      def not_found!
        return api_error(status: 404, errors: 'Not found')
      end

      def api_error(status: 500, errors: [])
        unless Rails.env.production?
          puts errors.full_messages if errors.respond_to? :full_messages
        end
        head status: status and return if errors.empty?

        render json: jsonapi_format(errors).to_json, status: status
      end

      def paginate(resource)
        resource = resource.page(params[:page] || 1)
        if params[:per_page]
          resource = resource.per_page(params[:per_page])
        end

        return resource
      end

      #expects pagination!
      def meta_attributes(object)
        {
          current_page: object.current_page,
          next_page: object.next_page,
          prev_page: object.previous_page,
          total_pages: object.total_pages,
          total_count: object.total_entries
        }
      end

      def authenticate_user!
          authenticate_with_http_token do |token, options|
              @user = User.find_by(authentication_token: token)
          end
          if @user
            @current_user = @user
           if params[:switch_tenant].present?
                Apartment::Tenant.switch!(params[:switch_tenant])
           else
                 if Apartment::Tenant.current != @current_user.tenant_name
                    Apartment::Tenant.switch!(@current_user.tenant_name) # Revert to the primary tenant
                end
           end
          else
            return unauthenticated!
          end
      end

      def check_tenant!
          if @current_user
                if Apartment::Tenant.current != @current_user.tenant_name
                  Apartment::Tenant.switch!(@current_user.tenant_name)
                elsif @current_user
                  Apartment::Tenant.switch! # Revert to the primary tenant
                end
          else
            error
          end
      end

      private

      #ember specific :/
      def jsonapi_format(errors)
        return errors if errors.is_a? String
        errors_hash = {}
        errors.messages.each do |attribute, error|
          array_hash = []
          error.each do |e|
            array_hash << {attribute: attribute, message: e}
          end
          errors_hash.merge!({ attribute => array_hash })
        end

        return errors_hash
      end
end

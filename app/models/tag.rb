class Tag < ActiveRecord::Base
    has_many :taggings
    has_many :gallery_images, through: :taggings
end

class CmsPage < ActiveRecord::Base
      before_save   :downcase_page_name
      validates :page_name,  presence: true,uniqueness: { case_sensitive: false }
      validates :title,  presence: true
      validates :description,  presence: true

      private

            # Converts email to all lower-case.
            def downcase_page_name
              self.page_name = page_name.downcase
            end
end

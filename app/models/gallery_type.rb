class GalleryType < ActiveRecord::Base
    #GEMS USED
    #ACCESSORS
    #ASSOCIATIONS
    has_many :gallery_images, class_name:"GalleryImage"
    #VALIDATIONS
    validates :name, :presence => true, :uniqueness => true
    validates :image_type, :presence => true, :uniqueness => true
    validates :width, :presence => true
    validates :height, :presence => true
    #CALLBACKS
    before_save :downcase_fields
    #SCOPES
    #CUSTOM SCOPES
    #OTHER METHODS
    #JOBS
    #PRIVATE 
    def as_json(options)
        super.merge(:include=>[:gallery_images])
      end
      
    def downcase_fields
      self.name.downcase!
      self.image_type.downcase!
    end
end

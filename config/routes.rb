Rails.application.routes.draw do

     mount Ckeditor::Engine => '/ckeditor'
      root                'static_pages#home'
      resources :account_activations, only: [:edit]
      resources :password_resets,     only: [:new, :create, :edit, :update]

      #api
      namespace :api do
        namespace :v1 do
          namespace :consumer do
             resources :cms_pages, only: [:index,:show]
             resources :gallery_types, only: [:index,:show]
             resources :gallery_images, only: [:index,:show]
          end
          resources :gallery_types 
          resources :gallery_images do
              collection { post :sort }
          end
          resources :cms_pages
          resources :settings
          resources :account_activations, only: [:edit,:create]
          resources :password_resets,     only: [:new, :create, :edit, :update, :show]
          resources :users, only: [:index, :create, :show, :update, :destroy]
          resources :sessions, only: [:create]
        end
      end
end
